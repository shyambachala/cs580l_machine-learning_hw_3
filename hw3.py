import pandas as pd
import numpy as np
import itertools


def count_generator(class_doc):
    class_dict = {}
    for each in class_doc['Message']:
        word_l = each.split(' ')
        for word in word_l:
            if word not in class_dict:
                class_dict[word] = 0
            class_dict[word] += 1
    return class_dict

def attach_class_train(train):
    c_1 = []
    for _ in itertools.repeat(None, 123):
        c_1.append('0')
    for _ in itertools.repeat(None, 340):
        c_1.append('1')
    train['Class'] = c_1
    return train

def attach_class_test(test):
    c_2 = []
    for _ in itertools.repeat(None, 130):
        c_2.append('0')
    for _ in itertools.repeat(None, 348):
        c_2.append('1')
    test['Class'] = c_2
    return test

def convert_to_numpy(df, f):
#    df['threshold'] = 1
#    f.append('threshold')
    f_m = np.array(df[f])
    f_m = np.float64(f_m)
    c_m = np.array(df['Class'])
    c_m = np.float64(c_m)
    return (f_m, c_m)

def predict_output(data_row, weights):
    # Take dot product of feature_matrix and coefficients
    #score = 0
    #for i in range(data_row.shape[0]):
        #score += data_row[i]*weights[i]
    score = (data_row*weights).sum(0)
    if score > 0:
        return 1
    else:
        return 0
    # return predictions

def perceptron_algorithm(df_x, df_y, learning_rate, loop_run):
    w = np.zeros(df_x.shape[1])
    for i in range(loop_run):
        for j in range(df_x.shape[0]):
            target_value = df_y[j]
            original_output = predict_output(df_x[j], w)
            if target_value != original_output:
                for k in range(w.shape[0]):
                    w[k] = w[k] + learning_rate*(target_value - original_output)*df_x[j][k]
    return w

def get_accuracy(df_x, df_y, w):
    class_l = []
    class_o = []
    class_count = 0
    for i in range(df_x.shape[0]):
        p = predict_output(df_x[i], w)
        class_l.append(p)
        class_o.append(int(df_y[i]))
        if p == int(df_y[i]):
            class_count += 1
    return class_count/df_x.shape[0]


desired_width = 320
pd.set_option('display.width', desired_width)


#READING FROM THE MODIFIED FILES
train_set = pd.read_csv('D:\\machineLearning\\hw2\\sample_code_test\\project_code\\sbachal1_python_hw2\\train\\train_modified.txt', names=['Message'])
test_set = pd.read_csv('D:\\machineLearning\\hw2\\sample_code_test\\project_code\\sbachal1_python_hw2\\test\\test_modified.txt', names=['Message'])

#TRAIN AND TEST ON THE ALGORITHM WITHOUT REMOVING THE STOP WORDS
global_vocab_train = count_generator(train_set)
global_vocab_test = count_generator(test_set)

common_features = []

for key in global_vocab_train:
    for jey in global_vocab_test:
        if jey == key:
            common_features.append(key)

#print(global_vocab)
#print(len(global_vocab))

train_set['threshold'] = 1
for global_term in common_features:
    train_set[global_term] = train_set['Message'].apply(lambda x: x.count(global_term))

train_df = attach_class_train(train_set)

#print(train_df)


features = train_df.columns.tolist()
features.remove('Message')
features.remove('Class')

y_train_df = train_df['Class']
x_train_df = train_df[features]

x_train_df, y_train_df = convert_to_numpy(train_df, features)
#print(x_train_df)
#print(y_train_df)


test_set['threshold'] = 1
for global_term in common_features:
    test_set[global_term] = test_set['Message'].apply(lambda x: x.count(global_term))

test_df = attach_class_test(test_set)

#print(train_df[:10])
#print(test_df[:10])
#print(test_df)
y_test_df = test_df['Class']
x_test_df = test_df[features]

x_test_df, y_test_df = convert_to_numpy(test_df, features)
##############################################################################
j = 0.01
while (j <= 0.2):
    weight_vector = perceptron_algorithm(x_train_df, y_train_df, learning_rate=j, loop_run=200)
    print(j, get_accuracy(x_train_df, y_train_df, weight_vector))
    j += 0.005

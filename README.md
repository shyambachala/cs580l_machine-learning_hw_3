##MACHINE LEARNING | IMPLEMENTATION OF THE MCAP LOGISTIC REGRESSION ALGORITHM WITH L2 REGULARIZATION AND PERCEPTRON ALGORITHM

##Project Description

Code Language: Python

Libraries Used: Numpy and Pandas.

•	Implemented MCAP Logistic Regression algorithm with L2 regularization.

•	Five diﬀerent values of λ (constant that determines the strength of the regularization term) are used to observe changes in results.

•	Used the algorithm to learn from the training set and report accuracy on the test set for diﬀerent values of λ. 

•	Implemented gradient ascent for learning the weights.  

•	Here the gradient ascent is run until a hard limit number of iterations is reached.

•	Tried to improve the algorithm by throwing away (i.e., ﬁltering out) stop words such as “the” “of” and “for” from all the documents. 

•	Implemented the Perceptron algorithm using the perceptron training rule.

•	Experimented with diﬀerent values of number of iterations and the learning rate. 

•	Tried to improve the algorithm by throwing away (i.e., ﬁltering out) stop words such as “the” “of” and “for” from all the documents. 

Results:

With stop-words:

•	Of all the algorithms, I observed that the Naive Bayes works well even with less training data, as the estimates are based on the joint density function.

•	Logistic regression with the small training data, model estimates over-fits the data.

•	With the perceptron algorithm it showed less accuracy as it needs the curve to reach convergence and has lot of complexity.

Without stop-words:

•	The accuracy decreases as compared to that of containing the stopwords as it might be that the some of the stopwords does contribute to the spam/ham detection i.e they have some significance in the detection thus when the stopwords that have significance are removed decreases the accuracy.
